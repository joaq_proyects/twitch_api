
import os, re

class Logger():
    log_folder = None
    log_file = None
    f_handler = None

    def __init__(self, log_folder, log_file):
        self.log_folder = log_folder
        self.log_file = log_file

    def next_log_n(self):
        pattern = re.compile(self.log_file + '.' + "(\d+)")
        next = 1
        items = os.listdir(self.log_folder)
        logs_n = list()
        for item in items:
            exp = re.search(pattern, item)
            if(exp):
                logs_n.append(int(exp.group(1)))
        if(len(logs_n) > 0):
            next = max(logs_n) + 1
        return self.log_file + '.' + str(next)


    def start_logger(self, new = False):
        mode = 'a'
        if(new):
            mode = 'w'
        self.f_handler = open(self.log_folder + "/" + self.next_log_n(), mode)

    def stop_logger(self):
        self.f_handler.close()
        self = None

    def log(self, text):
        try:
            self.f_handler.write(text)
        except:
            print("Error at writing log")
            self = None
            pass
