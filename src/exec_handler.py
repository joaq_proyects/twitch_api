
import datetime
import twitch_api, data_layer, settings, logger

class ExecHandler():
    client_id = None
    client_secret = None
    db_data = None
    dl_obj = None
    t_api = None
    logger_obj = None

    def __init__(self):
        self.client_id = settings.api_data["client_id"]
        self.client_secret = settings.api_data["client_secret"]
        self.db_data = settings.db_data
        self.dl_obj = data_layer.DataLayer(self.db_data["host"], self.db_data["user"], self.db_data["pssw"], self.db_data["db"])
        self.t_api = twitch_api.TwitchApi(self.client_id, self.client_secret)
        if(settings.log_data["enable"]):
            self.logger_obj = logger.Logger(settings.log_data["log_folder"], settings.log_data["log_file"])

    # def __del__(self):
        # print("handler destructor")
        # self.dl_obj.close()

    def new_token(self):
        self.t_api.new_token()

    def kill_instance(self):
        self.dl_obj.close()
        del self

    def is_token_valid(self):
        return self.t_api.is_token_valid()

    def start_logger(self, new = False):
        if(self.logger_obj is not None):
            self.logger_obj.start_logger(new)

    def stop_logger(self):
        if(self.logger_obj is not None):
            self.logger_obj.stop_logger()

    def log(self, text):
        if(self.logger_obj is not None):
            self.logger_obj.log(text)

    def api_pagination( self, get_method, check_method, insert_method, update_method, first ):
        antiloop_counter = 0
        antiloop_limit = 100000000 # THIS IS FOR AVOIDING AN INFINITE OR PUTTING LIMIT TO LOOP. MAY BE IT IS NOT NECESARY... VERY USEFULL FOR TESTING
        self.log("->api call:\t\t\t\t" + str(datetime.datetime.now()) + "\n")
        list = get_method(first)
        self.log("->api response:\t\t\t\t" + str(datetime.datetime.now()) + "\n")
        pagination = list.get("pagination", {}).get("cursor", None)
        data_list = list.get("data", [])
        while(pagination is not None and antiloop_counter < antiloop_limit):
            antiloop_counter += 1
            self.log("->insert/update " + str(len(data_list)) + " loop start:\t\t" + str(datetime.datetime.now()) + "\n")
            for elem in data_list:
                if( check_method(elem["id"]) ):
                    update_method(elem)
                else:
                    insert_method(elem)
            self.log("->insert/update loop end:\t\t" + str(datetime.datetime.now()) + "\n")
            self.log("->api call:\t\t\t\t" + str(datetime.datetime.now()) + "\n")
            list = get_method(first, pagination)
            self.log("->api response:\t\t\t\t" + str(datetime.datetime.now()) + "\n")
            pagination = list.get("pagination", {}).get("cursor", None)
            data_list = list.get("data", [])


    def api_sublisting_ids(self, select_method, get_method, check_method, insert_method, update_method):
        ids = select_method()
        print("ids:" + str(ids))
        list_ids_list = list()
        limit = 90 # api limit for list of ids (100)
        count = 0
        reg = -1
        for id in ids:
            if(count == 0 or count >= limit ):
                list_ids_list.append(list())
                count = 0
                reg += 1
            count += 1
            list_ids_list[reg].append(id)
        for ids_list in list_ids_list:
            self.log("->api call:\t\t\t\t" + str(datetime.datetime.now()) + "\n")
            data_list = (get_method(ids_list)).get("data", [])
            self.log("->api response:\t\t\t\t" + str(datetime.datetime.now()) + "\n")
            self.log("->insert/update loop start:\t\t" + str(datetime.datetime.now()) + "\n")
            for data in data_list:
                if(sub_objects_methods is not None):
                    aux_data = sub_objects_methods["get_data"](data["id"])
                    data = sub_objects_methods["refresh_data"](data, aux_data)
                if(check_method(data["id"])):
                    update_method(data)
                else:
                    insert_method(data)
            self.log("->insert/update loop end:\t\t" + str(datetime.datetime.now()) + "\n")



    def loop_over_ids_list(self, select_method, get_method, insert_method):
        ids = select_method()
        for id in ids:
            self.log("->api call:\t\t\t\t" + str(datetime.datetime.now()) + "\n")
            data = get_method(id)
            data["id"] = id
            self.log("->api response:\t\t\t\t" + str(datetime.datetime.now()) + "\n")
            insert_method(data)



    def call_streams_api(self):
        print("call_streams_api " + str(datetime.datetime.now()))
        self.start_logger(True)
        self.log("#call_streams_api:\t\t" + str(datetime.datetime.now()) + "\n")
        get_method = self.t_api.get_streams
        check_method = self.dl_obj.check_stream_id
        insert_method = self.dl_obj.insert_streams
        update_method = self.dl_obj.update_streams
        first = 90 # api limit for list of ids (100)
        self.api_pagination(get_method, check_method, insert_method, update_method, first)
        self.stop_logger()


    def call_games_api(self):
        print("call_games_api " + str(datetime.datetime.now()))
        self.start_logger()
        self.log("#call_games_api:\t\t" + str(datetime.datetime.now()) + "\n")
        select_method = self.dl_obj.select_games_id
        get_method = self.t_api.get_games
        check_method = self.dl_obj.check_user_id
        insert_method = self.dl_obj.insert_games
        update_method = self.dl_obj.update_games
        self.api_sublisting_ids(select_method, get_method, check_method, insert_method, update_method)
        self.stop_logger()


    def add_followers(self, main_data, aux_data):
        main_data["total_followers"] = aux_data["total"]
        main_data["total_followers_date"] = str(datetime.datetime.now())
        return main_data


    def call_users_api(self):
        print("call_users_api " + str(datetime.datetime.now()))
        self.start_logger()
        self.log("#call_users_api:\t\t" + str(datetime.datetime.now()) + "\n")
        select_method = self.dl_obj.select_users_id
        get_method = self.t_api.get_users
        check_method = self.dl_obj.check_user_id
        insert_method = self.dl_obj.insert_users
        update_method = self.dl_obj.update_users
        self.api_sublisting_ids(select_method, get_method, check_method, insert_method, update_method)
        self.stop_logger()


    def call_users_followers_api(self):
        print("call_users_followers_api " + str(datetime.datetime.now()))
        self.start_logger()
        self.log("#call_users_followers_api:\t\t" + str(datetime.datetime.now()) + "\n")
        select_method = self.dl_obj.select_all_users_id
        get_method = self.t_api.get_user_followers
        # check_method = self.dl_obj.check_user_followers_id
        insert_method = self.dl_obj.insert_user_followers
        # update_method = self.dl_obj.update_user_followers
        self.loop_over_ids_list(select_method, get_method, insert_method)
        self.stop_logger()
