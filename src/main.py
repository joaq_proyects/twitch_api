
import time
import schedule
import exec_handler, settings

def on_execution():
    print("-----------------")
    print("Begins scheduling")
    while(True):
        schedule.run_pending()
        time.sleep(10)


def execution_scheduler():
    try:
        print("execution_scheduler")
        jobs_sch_obj = settings.jobs_schedule
        exec_handler_obj = exec_handler.ExecHandler()
        jobs_sch_obj["streams"]["method"] = exec_handler_obj.call_streams_api
        jobs_sch_obj["games"]["method"] = exec_handler_obj.call_games_api
        jobs_sch_obj["users"]["method"] = exec_handler_obj.call_users_api
        jobs_sch_obj["users_followers"]["method"] = exec_handler_obj.call_users_followers_api


        def check_token_expiration():
            # print("check_token_expiration")
            if(not exec_handler_obj.is_token_valid()):
                exec_handler_obj.new_token()

        jobs_sch_obj["token"]["method"] = check_token_expiration

        # execute once, then scheduled
        jobs_sch_obj["streams"]["method"]()
        jobs_sch_obj["token"]["method"]()
        jobs_sch_obj["games"]["method"]()
        jobs_sch_obj["token"]["method"]()
        jobs_sch_obj["users"]["method"]()
        jobs_sch_obj["token"]["method"]()
        jobs_sch_obj["users_followers"]["method"]()

        for jkey in jobs_sch_obj:
            job = jobs_sch_obj[jkey]
            if(job["type"] == "daily"):
                schedule.every().day.at(job["hour"]).do(job["method"])
            if(job["type"] == "hourly"):
                schedule.every().hour.do(job["method"])
            if(job["type"] == "minutely"):
                schedule.every(int(job["each"])).minutes.do(job["method"])

        on_execution()
    except Exception as ex:
        print("An error ocurred during execution:" + str(ex))
        exit()

if __name__ == "__main__":
    execution_scheduler()
