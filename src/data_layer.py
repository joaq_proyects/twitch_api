
import data_access as da
import datetime

class DataLayer():

    da_obj = None

    def __init__(self, host, user, pssw, db):
        self.da_obj = da.DataAccess(host, user, pssw, db)
        print("New Connection")

    def __del__(self):
        self.da_obj.close()

    def close(self):
        del self
        print("Connection Closed")

    def replace_tags(self, text, tag, data):
        data_keys = data.keys()
        for dk in data_keys:
            replace_with = str(data[dk]).replace("'", "''")
            if(len(replace_with) == 0):
                replace_with = "null"
            text = text.replace(tag + dk + tag, replace_with)
        return text


    def exec_insert(self, query):
        self.da_obj.exec_non_query(query)
        self.da_obj.commit();


    def exec_update(self, query):
        self.da_obj.exec_non_query(query)
        self.da_obj.commit();


    def insert_streams(self, data):
        query = """
        insert into twitch_streams(
            id,
            community_ids,
            game_id,
            language,
            started_at,
            tag_ids,
            thumbnail_url,
            title,
            type,
            user_id,
            user_name,
            viewer_count
        )
        values(
            ##id##,
            '##community_ids##',
            ##game_id##,
            '##language##',
            '##started_at##',
            '##tag_ids##',
            '##thumbnail_url##',
            '##title##',
            '##type##',
            ##user_id##,
            '##user_name##',
            ##viewer_count##
        )
        """
        query = self.replace_tags(query, "##", data)
        return self.exec_insert(query)


    def update_streams(self, data):
        query = """
        update twitch_streams
        set community_ids = '##community_ids##',
            game_id = ##game_id##,
            language = '##language##',
            started_at = '##started_at##',
            tag_ids = '##tag_ids##',
            thumbnail_url = '##thumbnail_url##',
            title = '##title##',
            type = '##type##',
            user_id = ##user_id##,
            user_name = '##user_name##',
            viewer_count = ##viewer_count##
        where id = ##id##
        """
        query = self.replace_tags(query, "##", data)
        return self.exec_update(query)


    def insert_users(self, data):
        query = """
        insert into twitch_users(
        	id,
        	broadcaster_type,
        	description,
        	display_name,
        	login,
        	offline_image_url,
        	profile_image_url,
        	type,
        	view_count
        )
        values(
            ##id##,
            '##broadcaster_type##',
            '##description##',
            '##display_name##',
            '##login##',
            '##offline_image_url##',
            '##profile_image_url##',
            '##type##',
            '##view_count##'
        );
        """
        query = self.replace_tags(query, "##", data)
        return self.exec_insert(query)


    def update_users(self, data):
        query = """
        update twitch_users
        set broadcaster_type = '##broadcaster_type##',
        	description = '##description##',
        	display_name = '##display_name##',
        	login = '##login##',
        	offline_image_url = '##offline_image_url##',
        	profile_image_url = '##profile_image_url##',
        	type = '##type##',
        	view_count = ##view_count##
        where id = ##id##
        """
        query = self.replace_tags(query, "##", data)
        return self.exec_update(query)


    def insert_games(self, data):
        query = """
        insert into twitch_games(
            id,
            name,
            box_art_url
        )
        values(
            ##id##,
            '##name##',
            '##box_art_url##'
        )
        """
        query = self.replace_tags(query, "##", data)
        return self.exec_insert(query)

    def update_games(self, data):
        query = """
        update twitch_users
        set name = '##name##',
            box_art_url = '##box_art_url##'
        where id = ##id##
        """
        query = self.replace_tags(query, "##", data)
        return self.exec_update(query)


    def insert_user_followers(self, data):
        query = """
        insert into twitch_users_followers(
            id_user,
            total_followers
        )
        values(
            ##id##,
            ##total##
        )
        """
        query = self.replace_tags(query, "##", data)
        return self.exec_insert(query)


    def update_user_followers(self, data):
        query = """
        update twitch_users_followers
        set total_followers = ##total##,
            total_followers_date = '%s'
        where id_user = ##id##
        """ % (datetime.datetime.now(),)
        query = self.replace_tags(query, "##", data)
        return self.exec_update(query)


    def select_users_id(self):
        query = """
        SELECT twitch_streams.user_id
        FROM twitch_streams
        LEFT JOIN twitch_users ON twitch_streams.user_id = twitch_users.id
        where twitch_users.id IS NULL
        GROUP BY twitch_streams.user_id
        """
        table = self.da_obj.exec_query(query)
        res = list()
        for reg in table:
            res.append(reg[0])
        return res


    def select_all_users_id(self):
        query = """
        SELECT id
        FROM twitch_users
        """
        table = self.da_obj.exec_query(query)
        res = list()
        for reg in table:
            res.append(reg[0])
        return res



    def select_games_id(self):
        query = """
        SELECT twitch_streams.game_id
        FROM twitch_streams LEFT JOIN
        twitch_games ON twitch_streams.game_id = twitch_games.id
        where twitch_games.id IS NULL
        GROUP BY twitch_streams.game_id
        """
        table = self.da_obj.exec_query(query)
        res = list()
        for reg in table:
            res.append(reg[0])
        return res


    def check_stream_id(self, id):
        query = "select 1 from twitch_streams where id = " + str(id)
        table = self.da_obj.exec_query(query)
        return (len(table) > 0)

    def check_user_id(self, id):
        query = "select 1 from twitch_users where id = " + str(id)
        table = self.da_obj.exec_query(query)
        return (len(table) > 0)

    def check_user_followers_id(self, id):
        query = "select 1 from twitch_users_followers where id_user = " + str(id)
        table = self.da_obj.exec_query(query)
        return (len(table) > 0)
