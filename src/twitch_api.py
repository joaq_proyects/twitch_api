
import json, datetime
import requests

class TwitchApi():
    client_id = None
    client_secret = None
    grant_type = None
    token = None
    token_expire_date = None


    def __init__(self, client_id, client_secret, grant_type = "client_credentials"):
        self.client_id = client_id
        self.client_secret =  client_secret
        self.grant_type = grant_type
        self.new_token()

    def is_token_valid(self):
        return ( datetime.datetime.now() <= self.token_expire_date )

    def new_token(self):
        dtnow = datetime.datetime.now()
        token_data = self.get_token()
        self.token = token_data["access_token"]
        self.token_expire_date = dtnow + datetime.timedelta(seconds = token_data["expires_in"])
        # print("TOKEN:" + str(self.token))
        print("New Token till: " + str(self.token_expire_date))

    def get_token(self):
        try:
            service_url = "https://id.twitch.tv/oauth2/token?client_id=%s&client_secret=%s&grant_type=%s" % (self.client_id, self.client_secret, self.grant_type)
            req = requests.post(service_url)
            if(req.status_code == 200):
                res = json.loads(req.text)
            else:
                raise Exception(str(res.status_code) + "::" + str(res.text))
        except Exception as ex:
            print(ex)
            res = {}
        return res


    def auth_request(self, service_url, method):
        try:
            headers = {'Authorization': "Bearer " + self.token}
            req = dict()
            res = dict()
            if(method.lower() == "post"):
                req = requests.post(service_url, headers = headers)
            elif(method.lower() == "get"):
                req = requests.get(service_url, headers = headers)
            if(req.status_code == 200):
                res = json.loads(req.text)
            else:
                raise Exception(str(req.status_code) + "::" + str(req.text))
        except Exception as ex:
            print(ex)
            res = {}
        return res

    def get_streams(self, first = 100, after = None):
        service_url = "https://api.twitch.tv/helix/streams?first=" + str(first)
        method = "get"
        if(after is not None):
            service_url += "&after=" + after
        res = self.auth_request(service_url, method)
        return res


    def get_games(self, ids = []):
        service_url = "https://api.twitch.tv/helix/games"
        method = "get"
        symb = '?'
        for id in ids:
            service_url += symb + "id=" + str(id)
            if(symb == '?'):
                symb = '&'
        res = self.auth_request(service_url, method)
        return res


    def get_users(self, ids = []):
        service_url = "https://api.twitch.tv/helix/users"
        method = "get"
        symb = '?'
        for id in ids:
            service_url += symb + "id=" + str(id)
            if(symb == '?'):
                symb = '&'
        res = self.auth_request(service_url, method)
        return res


    def get_user_followers(self, to_id):
        service_url = "https://api.twitch.tv/helix/users/follows?to_id=" + str(to_id)
        method = "get"
        res = self.auth_request(service_url, method)
        return res
