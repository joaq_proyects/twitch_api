
create table twitch_users_followers
(
	id_user int,
	total_followers int,
	date date default GETDATE()
	PRIMARY KEY(id_user,date)
);
